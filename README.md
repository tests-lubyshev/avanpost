# Тестовое задание от кампании Аванпост

Задача выполнялась по следующему [техническому заданию](./TASK.md).

## Установка

```bash
git clone https://gitlab.com/tests-lubyshev/avanpost.git
cd avanpost
```

## Подготовка

* Измените если надо файл настроек `.env`
* Заполните каталог `/examples` файлами, в которых и будет производиться поиск ключевого слова

## Запуск

Строим образ

```bash
docker build -t avanpost-test:v1 .
```

Запускаем образ

```bash
docker run -it -p 8080:8080 avanpost-test:v1
```

## Проверка

1. Если не укажем параметр `word` (`http://127.0.0.1:8080/files/search`), то
получим в ответ следующий json:

```json
{
  "success":false,
  "error":"invalid 'word' query param"
}
```

2. Если ключевое слово не найдено (`http://127.0.0.1:8080/files/search?word=туманныеоврагиненайдены`), то
   получим в ответ следующий json:
```json
{
  "success":true,
  "error":"no files with 'туманныеоврагиненайдены' words"
}
```

3. И, наконец, если ключевое слово найдено (`http://127.0.0.1:8080/files/search?word=туманные`), то
   получим в ответ искомый json:
```json
{
  "success":true,
  "files":[
    "/var/www/!jobs/interviews/avanpost/avanpost/examples/file1.txt"
  ]
}
```

4. Если отправить любой не `GET` запрос, а например `POST`:

```bash
curl -v -d "word=ляляля" -X POST http://127.0.0.1:8080/files/search
```

получаем `Bad Request`:
```
*   Trying 127.0.0.1:8080...
* Connected to 127.0.0.1 (127.0.0.1) port 8080 (#0)
> POST /files/search HTTP/1.1
> Host: 127.0.0.1:8080
> User-Agent: curl/7.81.0
> Accept: */*
> Content-Length: 17
> Content-Type: application/x-www-form-urlencoded
> 
* Mark bundle as not supporting multiuse
< HTTP/1.1 400 Bad Request
< Date: Tue, 23 Jan 2024 11:47:15 GMT
< Content-Length: 0
< 
* Connection #0 to host 127.0.0.1 left intact
```
