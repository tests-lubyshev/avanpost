package searcher_test

import (
	"avanpost/pkg/config"
	"avanpost/pkg/searcher"
	"github.com/stretchr/testify/assert"
	"io/fs"
	"os"
	"reflect"
	"sort"
	"testing"
)

var sp = string(os.PathSeparator)

func TestSearcher_Factory(t *testing.T) {
	wd, _ := os.Getwd()
	var cfg = config.Config{
		DataDir:  "/examples",
		LogLevel: "DEBUG",
		RootDir:  wd + sp + ".." + sp + "..",
	}

	t.Run("OK", func(t *testing.T) {
		s, err := searcher.NewSearcher(searcher.SearchTypeFS, map[string]interface{}{
			searcher.ConfSearchDir:        cfg.RootDir + cfg.DataDir,
			searcher.ConfMaxSearchWorkers: int32(cfg.MaxSearchWorkers),
		})
		assert.NoError(t, err)
		assert.NotNil(t, s)
	})

	t.Run("FAIL SEARCH TYPE", func(t *testing.T) {
		s, err := searcher.NewSearcher(searcher.SearchTypeFS+"222", map[string]interface{}{
			searcher.ConfSearchDir:        cfg.RootDir + cfg.DataDir,
			searcher.ConfMaxSearchWorkers: int32(cfg.MaxSearchWorkers),
		})
		assert.Error(t, err)
		assert.Nil(t, s)
	})

	t.Run("FAIL CONFIG", func(t *testing.T) {
		s, err := searcher.NewSearcher(searcher.SearchTypeFS, nil)
		assert.Error(t, err)
		assert.Nil(t, s)
	})

}

func TestSearcher_SearchFS(t *testing.T) {
	type fields struct {
		FS fs.FS
	}
	type args struct {
		word string
	}

	wd, _ := os.Getwd()
	var cfg = config.Config{
		DataDir:          "/examples",
		LogLevel:         "DEBUG",
		RootDir:          wd + sp + ".." + sp + "..",
		MaxSearchWorkers: 2,
	}
	var path = cfg.RootDir + cfg.DataDir + sp

	t.Run("FAIL CONFIG DATA DIR", func(t *testing.T) {
		s, err := searcher.NewSearcher(searcher.SearchTypeFS, map[string]interface{}{
			searcher.ConfSearchDir:        cfg.RootDir + cfg.DataDir + "fail",
			searcher.ConfMaxSearchWorkers: int32(cfg.MaxSearchWorkers),
		})
		assert.NoError(t, err)
		assert.NotNil(t, s)

		_, err = s.Search("aaa")
		assert.Error(t, err)
	})

	// only for coverage )))
	t.Run("FAIL CONFIG WOKERS", func(t *testing.T) {
		s, err := searcher.NewSearcher(searcher.SearchTypeFS, map[string]interface{}{
			searcher.ConfSearchDir:        cfg.RootDir + cfg.DataDir,
			searcher.ConfMaxSearchWorkers: int32(0),
		})
		assert.NoError(t, err)
		assert.NotNil(t, s)
	})

	tests := []struct {
		name      string
		args      args
		wantFiles []string
		wantErr   bool
	}{
		{
			name:      "Ok",
			args:      args{word: "меня"},
			wantFiles: []string{path + "file2.txt", path + "file3.txt"},
			wantErr:   false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s, err := searcher.NewSearcher(searcher.SearchTypeFS, map[string]interface{}{
				searcher.ConfSearchDir:        cfg.RootDir + cfg.DataDir,
				searcher.ConfMaxSearchWorkers: int32(cfg.MaxSearchWorkers),
			})

			gotFiles, err := s.Search(tt.args.word)
			if (err != nil) != tt.wantErr {
				t.Errorf("Search() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			sort.Strings(gotFiles)
			if !reflect.DeepEqual(gotFiles, tt.wantFiles) {
				t.Errorf("Search() gotFiles = %v, want %v", gotFiles, tt.wantFiles)
			}
		})
	}
}
