package log

import (
	"github.com/rs/zerolog"
	"os"
	"strings"
)

var logContext zerolog.Context

func init() {
	zerolog.MessageFieldName = "MESSAGE"
	zerolog.LevelFieldName = "LEVEL"
	zerolog.ErrorFieldName = "ERROR"
	zerolog.TimestampFieldName = "TIME"
	zerolog.CallerFieldName = "CALLER"
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnixNano
}

func InitZeroLog(appName, logLevel string) {
	l, err := zerolog.ParseLevel(strings.ToLower(logLevel))
	if err != nil || l == zerolog.NoLevel {
		l = zerolog.InfoLevel
	}

	zerolog.SetGlobalLevel(l)

	logContext = zerolog.New(os.Stderr).With().Str("APP", appName)
}

func L() *zerolog.Logger {
	log := logContext.Timestamp().Caller().Logger()
	return &log
}
