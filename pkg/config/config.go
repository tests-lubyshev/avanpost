package config

import (
	"fmt"
	"github.com/joho/godotenv"
	"os"
	"strconv"
	"strings"
)

const dirSep = string(os.PathSeparator)

type Config struct {
	DataDir          string
	LogLevel         string
	RootDir          string
	MaxSearchWorkers int
	Host             string
	Port             int
}

func NewConfig() (*Config, error) {
	var err error
	var c Config
	if err = godotenv.Load(); err != nil {
		return nil, err
	}
	if c.RootDir, err = os.Getwd(); err != nil {
		return nil, err
	}
	if c.DataDir = os.Getenv("DATA_DIR"); c.DataDir != "" {
		c.DataDir = dirSep + strings.Trim(c.DataDir, dirSep)
	}
	c.LogLevel = os.Getenv("LOG_LEVEL")

	c.MaxSearchWorkers, err = strconv.Atoi(os.Getenv("SEARCH_WORKERS"))
	if err != nil || c.MaxSearchWorkers <= 0 {
		c.MaxSearchWorkers = 1
	}

	c.Host = os.Getenv("HTTP_HOST")
	if c.Host == "" {
		return nil, fmt.Errorf("param 'HTTP_HOST' is missed")
	}

	c.Port, err = strconv.Atoi(os.Getenv("HTTP_PORT"))
	if err != nil || c.Port <= 0 {
		return nil, fmt.Errorf("param 'HTTP_PORT' is missed or invalid: '%s'", os.Getenv("HTTP_PORT"))
	}

	return &c, nil
}
