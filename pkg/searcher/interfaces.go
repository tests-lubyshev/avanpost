package searcher

type Searcher interface {
	Search(word string) (entities []string, err error)
}
