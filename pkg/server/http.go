package server

import (
	"avanpost/pkg/log"
	"avanpost/pkg/searcher"
	"encoding/json"
	"fmt"
	"net/http"
)

type resp struct {
	Success bool     `json:"success"`
	Error   *string  `json:"error,omitempty"`
	Files   []string `json:"files,omitempty"`
}

func search(w http.ResponseWriter, req *http.Request, s searcher.Searcher) {
	if req.Method != http.MethodGet {
		log.L().Warn().Msg(fmt.Sprintf("wait 'GET' request, got '%s': skip", req.Method))
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	var r resp
	word := req.URL.Query().Get("word")
	if word == "" {
		var e = "invalid 'word' query param"
		r.Success = false
		r.Error = &e
	} else {
		entities, err := s.Search(word)
		if err != nil {
			var e = err.Error()
			r.Success = false
			r.Error = &e
		} else {
			r.Success = true
			if len(entities) > 0 {
				r.Files = entities
			} else {
				var e = fmt.Sprintf("no files with '%s' words", word)
				r.Error = &e
			}
		}
	}
	js, _ := json.Marshal(r)
	_, _ = w.Write(js)
}

func Start(host string, port int, searcher searcher.Searcher) error {
	http.HandleFunc("/files/search", func(w http.ResponseWriter, req *http.Request) {
		log.L().Debug().Msg("got 'files/search' request")
		search(w, req, searcher)
	})

	log.L().Info().Msg(fmt.Sprintf("start http-server at %s:%d", host, port))
	return http.ListenAndServe(fmt.Sprintf("%s:%d", host, port), nil)
}
