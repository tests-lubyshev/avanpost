package searcher

import (
	"avanpost/pkg/internal/dir"
	"avanpost/pkg/log"
	"fmt"
	"io/fs"
	"os"
	"strings"
	"sync"
	"sync/atomic"
	"time"
	"unicode"
)

const SearchTypeFS = SearchType("FS")

const (
	ConfSearchDir        = "SearchDir"
	ConfMaxSearchWorkers = "MaxSearchWorkers"
)

type fsSearcher struct {
	searchDir        string
	searchFrom       fs.FS
	maxSearchWorkers int32
}

func newFsSearcher(config map[string]interface{}) (Searcher, error) {
	var s fsSearcher

	if sd, ok := config[ConfSearchDir].(string); ok && sd != "" {
		s.searchDir = sd
		s.searchFrom = os.DirFS(sd)
	} else {
		err := fmt.Errorf("invalid search directory in config: '%s'", sd)
		log.L().Error().Err(err).Msg("")
		return nil, err
	}
	if sw, ok := config[ConfMaxSearchWorkers].(int32); ok && sw > 0 {
		s.maxSearchWorkers = sw
	} else {
		s.maxSearchWorkers = 1
	}

	return &s, nil
}

func (s *fsSearcher) Search(word string) (entities []string, err error) {
	var sr searchResult
	var wg sync.WaitGroup
	var cnt = s.maxSearchWorkers

	files, err := dir.FilesFS(s.searchFrom, ".")
	if err != nil {
		return nil, err
	}

	sd := s.searchDir + string(os.PathSeparator)
	for _, f := range files {
		wait(&cnt)
		wg.Add(1)
		go searchInFile(&wg, &cnt, sd+f, word, &sr)
	}
	wg.Wait()

	return sr.entities, nil
}

func wait(i *int32) {
	for atomic.LoadInt32(i) <= 0 {
		time.Sleep(time.Millisecond * 50)
	}
	atomic.AddInt32(i, -1)
}

type searchResult struct {
	entities []string
	mx       sync.Mutex
}

func (sr *searchResult) append(entity string) {
	defer sr.mx.Unlock()
	sr.mx.Lock()

	if sr.entities == nil {
		sr.entities = make([]string, 0)
	}
	sr.entities = append(sr.entities, entity)
}

func searchInFile(wg *sync.WaitGroup, cnt *int32, f string, word string, sr *searchResult) {
	defer func() {
		atomic.AddInt32(cnt, 1)
		wg.Done()
	}()
	log.L().Debug().Msg("start worker")

	data, err := os.ReadFile(f)
	if err != nil {
		log.L().Error().Err(err).Msg("fail to read data")
		return
	}

	words := strings.FieldsFunc(string(data), func(r rune) bool {
		return !unicode.IsLetter(r) && !unicode.IsDigit(r)
	})
	for _, str := range words {
		if word == str {
			log.L().Debug().Msgf("word '%s' found", word)
			sr.append(f)
			break
		}
	}

	log.L().Debug().Msg("finish worker")
}
