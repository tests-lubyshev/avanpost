package searcher

import "fmt"

type SearchType string

func NewSearcher(st SearchType, config map[string]interface{}) (s Searcher, err error) {
	switch st {
	case SearchTypeFS:
		return newFsSearcher(config)
	}

	return nil, fmt.Errorf("invalid searcher type")
}
