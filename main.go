package main

import (
	"avanpost/pkg/config"
	"avanpost/pkg/log"
	"avanpost/pkg/searcher"
	"avanpost/pkg/server"
	zl "github.com/rs/zerolog/log"
)

const appLogName = "avanpost-test"

func main() {
	cfg, err := config.NewConfig()
	if err != nil {
		zl.Fatal().Err(err).Caller().Str("APP", appLogName).Msg("fail to get config")
	}

	log.InitZeroLog(appLogName, cfg.LogLevel)
	log.L().Info().Msg("start Avanpost-Test app")

	s, err := searcher.NewSearcher(searcher.SearchTypeFS, map[string]interface{}{
		searcher.ConfSearchDir:        cfg.RootDir + cfg.DataDir,
		searcher.ConfMaxSearchWorkers: int32(cfg.MaxSearchWorkers),
	})
	if err != nil {
		log.L().Fatal().Err(err).Msg("fail to get searcher")
	}

	if err = server.Start(cfg.Host, cfg.Port, s); err != nil {
		log.L().Fatal().Err(err).Msg("http-server crushed")
	}

	log.L().Info().Msg("finish Avanpost-Test app")
}
